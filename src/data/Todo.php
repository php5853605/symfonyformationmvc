<?php

namespace App\data;

/*
 * model de données
 * il comprend 4 attributs
 * id qui est l'identifiant unique de chaque objet
 * task qui sera la tâche à réaliser
 * decription qui est une description de la tâche
 * completed qui défini si la tâche a été réalisé ou non
 * */
class Todo
{
    public int $id;
    public string $task;

    public string $description;
    public bool $completed;

    private static int $count = 1;

    public function __construct($task, $description)
    {
        $this->completed = false;
        $this->task = $task;
        $this->id = self::$count;
        $this->description = $description;
        self::$count++;
    }
}