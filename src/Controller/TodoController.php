<?php

// espace ou se trouveront tous les controller

namespace App\Controller;

// import des différentes classes pour le fonctionnement de mon controller
use App\data\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/*
 *  route par defaut de mon controller
 * /todo
 */
#[Route('/todo')]
/*
 * on etend abstract controller pour hériter de ses méthodes
 * ses méthodes permettront de faire de l'http et du render ainsi que tout ce qui sera attendu d'un controller
 * */
class TodoController extends AbstractController
{

    /*
     * route par defaut de todo
     * method get sous entend que je ne reçois rien du body de la requête
     * */
    #[Route('/', name: 'app.todo', methods: "GET")]
    public function index(Request $request): Response
    {
        // on récupère la session en cours
        $session = $request->getSession();

        // on récupère todolist dans la session
        $todolist = $session->get('todolist');

        // si todolist n'existe pas je crée la variable dans la session
        if ($todolist == null) {
            $session->set('todolist',$this->init());
        }
        // méthode permettant d'afficher le template depuis le controller
        // 2 paramètres sont renseignés (le path du template et un tableau associatif qui comprend les variables à envoyer au template)
        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
            'todolist' => $session->get('todolist')
        ]);
    }

    /*
     * route permettant d'afficher les details de notre todo
     * elle prend en paramètre un argument dynamique (ici l'id)
     * j'utilise l'objet request pour pouvoir gérer la session
     * son path sera localhost:8000/todo/detail/{numero}
     * */
    #[Route('/detail/{id}', name: 'app.todo.detail', methods: "GET")]
    public function detail(Request $request, int $id): Response
    {
        // on récupère la session en cours
        $session = $request->getSession();

        // on récupère todolist dans la session
        $todolist = $session->get('todolist');
        $result = null;
        // on parcours la todolist, si l'id est trouvée, la todo est retournée
        foreach ($todolist as $todo) {
            if($todo->id === $id) {
                $result = $todo;
            }
        }
        // dans le cas ou la todo n'a pas été trouvé, j'affiche une bannière avec un message custom
        if($result == null) {
            $this->addFlash('warning', 'La todo que vous tentez d\'afficher n\'existe pas');
        }
        // j'affiche le template avec comme paramètre, l'objet todo
        return $this->render('todo/detail.html.twig', [
            'controller_name' => 'TodoController',
            'todo' => $result
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * route permettant la suppression d'un todo
     */
    #[Route('/delete/{id}', name: 'app.todo.delete', methods: "GET")]
    public function delete(Request $request, int $id)
    {
       $session = $request->getSession();
        $todolist = $session->get('todolist');
        // je parcours la todolist, et je supprime la todo par son index ($key)
        foreach ($todolist as $key => $todo) {
            if ($todo->id == $id) {
                unset($todolist[$key]);
            }
        }
        // j'ecrase le tableau precedent par le nouveau dans la session
        $session->set('todolist', $todolist);

        // je fais une redirection vers la page par defaut de todo
        return $this->redirect("/todo");
    }

    /*
     * Route permettant le changement d'etat d'une todo
     * on récupère l'id dans l'url
     */
    #[Route('/patch/completed/{id}', name: 'app.todo.patch.completed', methods: "GET")]
    public function patchCompleted(Request $request, int $id): Response
    {
        $session = $request->getSession();
        $todolist = $session->get('todolist');

        foreach ($todolist as $todo) {
            if ($todo->id == $id) {
               $todo->completed = !$todo->completed;
            }
        }

        $session->set('todolist', $todolist);

        return $this->redirect('/todo');
    }

    /*
     * Méthode permettant d'initialiser les valeurs par défaut de notre todolist
     */
    private function init(): array
    {
        return [
            new Todo("Apprendre symfony", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati!"),
            new Todo("Créer un controller","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati!"),
            new Todo("Manipuler les données","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati!"),
        ];
    }
}
